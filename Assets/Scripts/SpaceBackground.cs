﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ObjectInfo
{
    public Texture texture;
    public int randomWeight;
    public int count;
    public float minSize, maxSize;
    public Color color1, color2;
    public int depth;
    public float brightness;
}

public class SpaceBackground : MonoBehaviour
{
    private GameObject prefab, spaceObjectContainer, player, spaceObjectLightPrefab;

    public ObjectInfo[] objectInfos;
    public int spaceObjectCount = 1000;

    void Start()
    {
        prefab = Resources.Load("space object") as GameObject;
        spaceObjectLightPrefab = Resources.Load("Space Object Light") as GameObject;
        spaceObjectContainer = new GameObject("space objects");
        player = FindObjectOfType<FPSInputController>().gameObject;

        foreach (ObjectInfo oi in objectInfos)
            if (oi.count > 0)
                for (int i = 0; i < oi.count; i++)
                    MakeSpaceObject(oi);

        for (int i = 0; i < spaceObjectCount; i++)
            MakeSpaceObject(GetRandomObjectInfo());
    }

    private ObjectInfo GetRandomObjectInfo()
    {
        int totalWeight = 0;
        foreach (ObjectInfo oi in objectInfos)
            totalWeight += oi.randomWeight;

        int r = Random.Range(0, totalWeight);
        int indexSoFar = 0;
        foreach (ObjectInfo oi in objectInfos)
        {
            indexSoFar += oi.randomWeight;
            if (r < indexSoFar)
                return oi;
        }
        return null;
    }

    private void MakeSpaceObject(ObjectInfo oi)
    {
        Vector3 position = Random.onUnitSphere * (10 + oi.depth+Random.Range(0,0.5f));
        GameObject newSpaceObject = Instantiate(prefab, position, Quaternion.identity) as GameObject;
        newSpaceObject.transform.parent = spaceObjectContainer.transform;
        newSpaceObject.transform.LookAt(Vector3.zero);

        Material mat = newSpaceObject.transform.GetChild(0).GetComponent<MeshRenderer>().material;
        mat.SetTexture(0, oi.texture);
        Color newColor = Color.Lerp(oi.color1, oi.color2, Random.Range(0f, 1f));
        newColor.a = 1f;
        mat.color = newColor;

        float newSize = Random.Range(oi.minSize, oi.maxSize);
        newSpaceObject.transform.localScale = new Vector3(newSize, newSize, newSize);

        if (oi.brightness > 0)
        {
            GameObject newLight = Instantiate(spaceObjectLightPrefab, position, Quaternion.identity) as GameObject;
            Light light = newLight.GetComponent<Light>();
            light.color = newColor;
            light.intensity = oi.brightness;
            newLight.transform.LookAt(Vector3.zero);
        }
    }

    void Update()
    {
        spaceObjectContainer.transform.position = player.transform.position;
    }
}
